package com.stormnet.inheritance;

public class Car extends Property {
    private int maxSpeed;
    private int engineVolume;
    private String tankType;
    private String transmission;

    private Car(String color, String vendor, int price) {
        super(color, vendor, price);
    }

    public Car(String color, String vendor, int price, int maxSpeed, String transmission, int engineVolume, String tankType) {
        super(color, vendor, price);
        this.maxSpeed = maxSpeed;
        this.transmission = transmission;
        this.engineVolume = engineVolume;
        this.tankType = tankType;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getTransmission() {
        return transmission;
    }

    public int getEngineVolume() {
        return engineVolume;
    }

    public String getTankType() {
        return tankType;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public void setEngineVolume(int engineVolume) {
        this.engineVolume = engineVolume;
    }

    public void setTankType(String tankType) {
        this.tankType = tankType;
    }

    @Override
    public String toString() {
        return "Car{" +
                "maxSpeed=" + maxSpeed +
                ", engineVolume=" + engineVolume +
                ", tankType='" + tankType + '\'' +
                ", transmission='" + transmission + '\'' +
                ", color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }

}
