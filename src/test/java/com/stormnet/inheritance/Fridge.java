package com.stormnet.inheritance;

public class Fridge extends Property {
    private int cameraCount;
    private int volume;
    private String energyConsumptionClass;

    private Fridge(String color, String vendor, int price) {
        super(color, vendor, price);
    }

    public Fridge(String color, String vendor, int price, int cameraCount, int volume, String energyConsumptionClass) {
        super(color, vendor, price);
        this.cameraCount = cameraCount;
        this.volume = volume;
        this.energyConsumptionClass = energyConsumptionClass;
    }

    public int getCameraCount() {
        return cameraCount;
    }

    public int getVolume() {
        return volume;
    }

    public String getEnergyConsumptionClass() {
        return energyConsumptionClass;
    }

    public void setCameraCount(int cameraCount) {
        this.cameraCount = cameraCount;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public void setEnergyConsumptionClass(String energyConsumptionClass) {
        this.energyConsumptionClass = energyConsumptionClass;
    }

    @Override
    public String toString() {
        return "Fridge{" +
                "cameraCount=" + cameraCount +
                ", volume=" + volume +
                ", energyConsumptionClass='" + energyConsumptionClass + '\'' +
                ", color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }

}
