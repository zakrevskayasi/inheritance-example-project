package com.stormnet.inheritance;

public class Table extends Property {
    private String material;
    private int size;

    private Table(String color, String vendor, int price) {
        super(color, vendor, price);
    }

    public Table(String color, String vendor, int price, String material, int size) {
        super(color, vendor, price);
        this.material = material;
        this.size = size;
    }

    public String getMaterial() {
        return material;
    }

    public int getSize() {
        return size;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Table{" +
                "material='" + material + '\'' +
                ", size=" + size +
                ", color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }

}
