package com.stormnet.inheritance;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Property {
    protected String color;
    protected String vendor;
    protected int price;

    public Property(String color, String vendor, int price) {
        this.color = color;
        this.vendor = vendor;
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public String getVendor() {
        return vendor;
    }

    public int getPrice() {
        return price;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Property{" +
                "color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }

    protected String getInfo(){
        Field[] fields = this.getClass().getDeclaredFields();
        Method[] methods = this.getClass().getDeclaredMethods();
        String fieldsNames = "";
        String methodsNames = "";

        for (Field f: fields){
            fieldsNames += f.getName() + ", ";
        }

        for (Method m: methods){
            methodsNames += m.getName() + ", ";
        }

        return this.getClass().getName() + " class has next members: \n" + "FIELDS: " + fieldsNames + "\n" + "METHODS: " + methodsNames;
    }

}
