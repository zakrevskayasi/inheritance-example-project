package com.stormnet.inheritance;

public class Laptop extends Property {

    private String characteristics;

    private Laptop(String color, String vendor, int price) {
        super(color, vendor, price);
    }

    public Laptop(String color, String vendor, int price, String characteristics) {
        super(color, vendor, price);
        this.characteristics = characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "characteristics='" + characteristics + '\'' +
                ", color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }

}
