package com.stormnet.inheritance;

public class MainClass {
    public static void main(String[] args) {
        Property[] properties = {
                new Car("red", "BMW", 25000, 200, "AKPP", 3, "petrol"),
                new Laptop("black", "Asus", 1000, "Intel(R) Core(TM) i5-4570 CPU 3.20GHz"),
                new Table("brown", "Пинскдрев", 200, "Wood", 2),
                new Fridge("white", "Atlant", 200, 2, 5, "A++")
        };

        for (Property p: properties){
            System.out.println(p.getInfo());
            System.out.println(p.toString());
            System.out.println("---------------------------------------------------------------------------------------------------------");
        }
    }
}
